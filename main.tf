provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

module "argocd" {
  source              = "./modules/argocd"

  git_base_url        = ""
  environment         = terraform.workspace
  slack_api_url       = ""
  ntfy_url            = ""
  cert_manager_email  = ""
  argocd_domain       = "argocd.${terraform.workspace}.chilicongraphics.com"
  ip_restriction      = ""
}
