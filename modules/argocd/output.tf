output "argocd-credentials" {
  value = random_password.argocd-admin-password.result
}