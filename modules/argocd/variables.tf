variable "argocd_helm_version" {
  default = "6.2.3"
}
variable "argocd_version" {
  default = "v2.10.1"
}
variable "argocd_css_nav_bar" {
  type = string
  default = "linear-gradient(0.25turn, #3f87a6, #ebf8e1, #f69d3c);"
}
variable "deploy_custom_css" {
  type = bool
  default = true
}
variable "serviceMonitor" {
  type = bool
  default = false
}

variable "git_base_url" {
  type = string
}

variable "environment" {
  type = string
}

variable "slack_api_url" {
  type = string
}

variable "ntfy_url" {
  type = string
}

variable "cert_manager_email" {
  type = string
}

variable "argocd_domain" {
  type = string
}

variable "ip_restriction" {
  type = string
}